detail.js

```js
import { request } from '../../request/index.js'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        // detail_pic:[
        //     {
        //         imgSrc:"../../image/detail_1.png",
        //     },
        //     {
        //         imgSrc:"../../image/detail_1.png",
        //     },
        //     {
        //         imgSrc:"../../image/detail_1.png",
        //     }

        // ],
        // houses_info: [
        //     {
        //         name: '房屋地址',
        //         info:'天津市西青区大学城工一号路'
        //     },
        //     {
        //         name: '小区名称',
        //         info:'姚村公寓'
        //     },
        //     {
        //         name: '房屋房型',
        //         info:'3室1厅1卫'
        //     },
        //     {
        //         name: '房屋楼层',
        //         info:'10楼  有电梯'
        //     },{
        //         name: '房屋距离',
        //         info:'与我距离  300km'
        //     }
        // ],
        // house_prime: [
        //     {
        //         id: 0,
        //         prime: '平日价格',
        //         prime_day:'100元/天'
        //     },
        //      {
        //         id: 1,
        //         prime: '周末价格',
        //         prime_day:'120元/天'
        //     },
        //       {
        //         id: 2,
        //         prime: '节日价格',
        //         prime_day:'150元/天'
        //     },
        //        {
        //         id: 3,
        //         prime: '整月价格',
        //         prime_day:'500元/天'
        //     },
        //         {
        //         id: 4,
        //         prime: '入住押金',
        //         prime_day:'100元'
        //     },
        //          {
        //         id: 5,
        //         prime: '平日价格',
        //         prime_day:'100元/天'
        //     },
        //           {
        //         id: 6,
        //         prime: '最小租期',
        //         prime_day:'1天'
        //     },
        //            {
        //         id: 7,
        //         prime: '额外费用',
        //         prime_day:'夏天空调每天10天，冬天取暖费每天10元，其他费用（无）'
        //     }
        // ],
         routers: [
      {
        icon: '../../image/people.png'
      },
      {
        icon: '../../image/bed.png'
      },
      {
        icon: '../../image/cook.png'
      },
       {
        icon:'../../image/matong.png' 
      },
       {
        icon:'../../image/house.png' 
      },
       {
        icon:'../../image/xishu.png' 
      },
       {
        icon:'../../image/zhentou.png' 
      },
       {
        icon:'' 
            },
    ],
    // 指定id的对象数据
    detailData:{}
    
    
    
   
    },
    /**
     * 生命周期函数--监听页面加载
     */
  // 详细页
  onLoad: function (options) {
    // console.log("获取传过来的值"+options);
    // 获取参数值
    let cid = parseInt(options.id);
    console.log("传过来得到的cid：" + cid);
    var that=this;
    wx.request({
      url: `http://127.0.0.1:3000/detail?id=${cid}`,
      success: (res) => {
        console.log(res)
        let data = res.data.detail.filter(item => {
          return item.id==cid
        })
        console.log(data[0]);
        that.setData({
          detailData:data[0]
          
        })
      }
    })
     
        // this.getDetailList();
      // wx.request({
      //   url: `http://127.0.0.1:3000/home?id=${id}`,
      //   data: {
      //     id:this.data.id
      //   },
      //   method: 'GET',
      //   success: function (res) {
      //     console.log(res);
      //   that.setData({  
      //     detailObj: res.data.data,
      //         })
      //   }
      // })
        // this.getDetailList();
  
    
  },
    // getDetailList() {
    //   request({ url: "/detail",data:{id:this.detail.id}})
    //     .then(result => {
    //           console.log(result);
    //          this.setData({
    //           detailObj:result.detail
    //           })
    //       })
    //     },
   
})
```

