# 如何打造高效稳定的私人图床：Node.js+PicGo+Typora+Gitee

1.安装node.js

2.创建gitee仓库

![image-20211010143529313](https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010143529313.png)

3.创建私人令牌

<img src="https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010113834485.png" alt="image-20211010113834485" style="zoom:120%;" />

![image-20211010143611444](https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010143611444.png)

Blog-img 私人令牌：`154e86069ae003e5c864084393f959d2` (私人存藏，不得公开)

保存私人令牌的token***（谨记：令牌只会出现一次，请务必妥善保管，建议整套搭配存放私人备忘录）***

4.安装picgo

<img src="https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010091750378.png" alt="image-20211010091750378" style="zoom:150%;" />

5.安装插件

![image-20211010110239161](https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010110239161.png)

6.picgo设置gitee

repo:用户名/仓库名

token:刚刚生成的私人令牌复制过来

非星号不用填。

![image-20211010110326528](https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010110326528.png)

7.配置镜像地址

`https://registry.npm.taobao.org/`

<img src="https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/gPYaZvKIJsXfjr8.png" alt="image-20211010093614746" style="zoom:120%;" />

8.设置Typoraq

<img src="https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010110544224.png" alt="image-20211010110544224" style="zoom:120%;" />

9.测试

![image-20211010110608163](https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010110608163.png)

typora设置-->偏好设置-->图像-->验证

出现绿色的文字说明已经成功！

<img src="https://gitee.com/hanrui-7/polaris/raw/master/%E5%A6%82%E4%BD%95%E6%89%93%E9%80%A0%E9%AB%98%E6%95%88%E7%A8%B3%E5%AE%9A%E7%9A%84%E7%A7%81%E4%BA%BA%E5%9B%BE%E5%BA%8A%EF%BC%9ANode.js+PicGo+Typora+Gitee/image-20211010111215083.png" alt="image-20211010111215083" style="zoom:50%;" />



