MongoDB:非关系型数据库

1、查看版本

cmd命令：mongo -version、mongod -vsersion

<img src="E:\typora\Typora图片\typora-user-images\image-20210917140634819.png" alt="image-20210917140634819" style="zoom: 67%;" />

2、启动服务

cmd命令：mongod

<img src="E:\typora\Typora图片\typora-user-images\image-20210917141130315.png" alt="image-20210917141130315" style="zoom:67%;" />

3、连接数据库

[需要新开启一个cmd，命令：mongo]()

<img src="E:\typora\Typora图片\typora-user-images\image-20210917141154735.png" alt="image-20210917141154735" style="zoom:67%;" />

4、查看数据库

命令：show dbs

<img src="E:\typora\Typora图片\typora-user-images\image-20210917141252372.png" alt="image-20210917141252372" style="zoom:67%;" /><img src="E:\typora\Typora图片\typora-user-images\image-20210917141903642.png" alt="image-20210917141903642" style="zoom:50%;" />

5、查看当前操作的数据库

db:如果当前没有使用数据库，则默认使用的是test数据库

<img src="E:\typora\Typora图片\typora-user-images\image-20210917141948345.png" alt="image-20210917141948345" style="zoom:50%;" />

**db使用的数据库如果没有数据则再查询数据库的时候不显示该数据库，需要插入数据才会显示出来**

6、使用数据库

use xxx（数据库名）:使用xxx数据库，如果没有xxx数据库，则会默认给创建出来

<img src="E:\typora\Typora图片\typora-user-images\image-20210917142420883.png" alt="image-20210917142420883" style="zoom: 50%;" />

使用命令行能创建出来，但是在工具不能。

<img src="E:\typora\Typora图片\typora-user-images\image-20210917145032606.png" alt="image-20210917145032606" style="zoom: 67%;" />

7、插入数据

```sql
db.集合名.insertOne({"name":"zhangsan"})
```

<img src="E:\typora\Typora图片\typora-user-images\image-20210917142750748.png" alt="image-20210917142750748" style="zoom: 67%;" />

<img src="E:\typora\Typora图片\typora-user-images\image-20210917142916190.png" alt="image-20210917142916190" style="zoom: 50%;" />

8、查看db当前数据库的集合

```sql
show collections
```

<img src="E:\typora\Typora图片\typora-user-images\image-20210917143537049.png" alt="image-20210917143537049" style="zoom: 50%;" />

9、查看当前集合下的数据

```sql
db.xxx.find()
```

<img src="E:\typora\Typora图片\typora-user-images\image-20210917143732658.png" alt="image-20210917143732658" style="zoom: 50%;" />

10、删除集合

```sql
db.xxx.drop() 返回：true
```

<img src="E:\typora\Typora图片\typora-user-images\image-20210917144241473.png" alt="image-20210917144241473" style="zoom:50%;" />

11、删除数据库

```sql
db.dropDatabase()	
```

<img src="E:\typora\Typora图片\typora-user-images\image-20210917144444235.png" alt="image-20210917144444235" style="zoom:50%;" />

<img src="E:\typora\Typora图片\typora-user-images\image-20210917144523895.png" alt="image-20210917144523895" style="zoom:50%;" />

总结命令行

| 1、总结命令行 |
| ------------- |

(1)    use tjise       使用/切换到某个数据库，如果没有则创建

(2)    db          查看当前操作的是哪个数据库

(3)    db.students.insertOne()  在当前数据库中创建students集合，并插入一条数据

(4)    show collections    查看当前数据库中的集合

(5)    db.students.find()    查看集合中的数据

(6)    Db.students.drop()   删除该集合

(7)    db.dropDatabase()   删除该数据库