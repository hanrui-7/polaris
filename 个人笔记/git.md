## 一、如何将本地项目推送到码云

步骤：

1、码云上新建一个项目 XXXX （项目名）

2、本地创建一个文件夹E:/XXXX，然后使用git bash

3、cd 到本地文件夹中E:/XXXX //如果是在创建的文件中git bash 则此步骤可省略（根目录下）

4、使用 git init 命令 //初始化一个git 本地仓库此时会在本地创建一个 .git 的文件夹

5、使用git remote add origin https://gitee.com/你的码云用户名/XXXX  //添加远程仓库

​	git remote -v 查看远程库信息

​    git remote rm origin(删除关联的origin的远程库)

6、使用 git pull origin master 命令，将码云上的仓库pull到本地文件夹

7、将要上传的文件，添加到刚刚创建的文件夹

8、使用git add . （. 表示所有的）或者 git add + 文件名 // 将文件保存到缓存区

9、使用git commit -m '新添加的文件内容描述' //添加文件描述

10、使用git push origin master ，将本地仓库推送到远程仓库





## 二、git 推送项目报错

![](https://gitee.com/hanrui-7/polaris/raw/master/git/20181122160512736.png)

如果已经git pull 后再推送发现还是报

![image-20211010151209805](https://gitee.com/hanrui-7/polaris/raw/master/git/image-20211010151209805.png)

解决办法：这是一个常见问题，要把两个不同的项目合并，需要添加一个强制命令

输入命令: git pull origin master --allow-unrelated-histories

![img](https://gitee.com/hanrui-7/polaris/raw/master/git/20181122160641294.png)

提示有冲突的文件;去到对应的里面修改好了再

查看状态:

```javascript
git status
```

输入命令: 

```javascript
 git add ./
```

输入命令: 

 

```javascript
git commit -m '新添加的文件内容描述'
```

输入命令:

```javascript
git push origin master
```

回到仓库里就会发现已更新

![image-20211010151456512](https://gitee.com/hanrui-7/polaris/raw/master/git/image-20211010151456512.png)

## 三、Git命令中文显示乱码的问题解决

使用git add添加要提交的文件的时候，如果文件名是中文，会显示形如274\232\350\256\256\346\200\273\347\273\223的乱码。 

解决方案：在bash提示符下输入： 

```
git config --global core.quotepath false
```

core.quotepath设为false的话，就不会对0x80以上的字符进行quote。中文显示正常。

## 四、如何删除暂存区文件

```js
git rm ./xxx
-r强制删除文件夹里的所有文件
```

## 五、如何撤回误操作

```json
git reset --hard Head~0
  表示回退到上一次代码提交时的状态
git reset --hard Head~1
  表示回退到上上次代码提交时的状态
git reset --hard Head~3
  表示回退到上上上次代码提交时的状态
git reset --hard [版本号]
  可以通过版本号精确的回退到某一次提交时的状态
git reflog
  可以看到每一次切换版本的记录：可以看到所有提交的版本号
```

