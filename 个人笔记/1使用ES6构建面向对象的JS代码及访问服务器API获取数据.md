1使用ES6构建面向对象的JS代码及访问服务器API获取数据

新建home   手动添加home-model.js

<img src="E:\typora\Typora图片\typora-user-images\image-20210929211859924.png" alt="image-20210929211859924" style="zoom:50%;" />

home-model.js

```js
// 定义一个Home类
class Home{
    //类自带的构造函数
    constructor(){
    }
    //定义一个getBannerData方法：请求数据
    getBannerData(){
        wx.request({
          url: 'http://127.0.0.1:3000/list',
          method:'GET',
          success:(res)=>{
              console.log(res);
              return res;
          }
        })
    }
}
export {Home}
```

home.js  绑定数据

```js
// pages/home/home.js
var home=new Home();
Page({
    /**
     * 页面的初始数据
     */
    data: {

    },
    onLoad:function(){
        this._loadData();
    },
    _loadData:function(){
        
        var home.getBannerDate();
    }

})
```

在home-model.js添加callBack   由于id 响应不出来 ，先放着或者去掉id

<img src="../Typora图片/typora-user-images/image-20210930085341614.png" alt="image-20210930085341614" style="zoom:70%;" />

在home.js中

<img src="../Typora图片/typora-user-images/image-20210930085442944.png" alt="image-20210930085442944" style="zoom:80%;" />

<img src="../Typora图片/typora-user-images/image-20210930085509999.png" alt="image-20210930085509999" style="zoom:67%;" />

home.js简化callBack回调函数



![image-20210930090124664](../Typora图片/typora-user-images/image-20210930090124664.png)

去掉id

<img src="../Typora图片/typora-user-images/image-20210930090536853.png" alt="image-20210930090536853" style="zoom:67%;" />

结果如下

![image-20210930085958526](../Typora图片/typora-user-images/image-20210930085958526.png)

在utils新建一个base.js 封装请求

<img src="../Typora图片/typora-user-images/image-20210930092853131.png" alt="image-20210930092853131" style="zoom:60%;" />

```js
class Base{
    constructor(){

    }
    request(){
        wx.request({
          url: 'http://127.0.0.1:3000/list',
          data:null,
          method:'',
          header:null,
          success:(res)=>{

          },
          fail:(err)=>{
              
          }
        })
    }
}
```

<img src="../Typora图片/typora-user-images/image-20210930095129465.png" alt="image-20210930095129465" style="zoom:70%;" />

  request方法：网络请求

<img src="../Typora图片/typora-user-images/image-20210930095430178.png" alt="image-20210930095430178" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930095530042.png" alt="image-20210930095530042" style="zoom:67%;" />

在utils中创建config.js

![image-20210930095632067](../Typora图片/typora-user-images/image-20210930095632067.png)

![image-20210930095853914](../Typora图片/typora-user-images/image-20210930095853914.png)

别忘记导出export和引入import

![image-20210930095956330](../Typora图片/typora-user-images/image-20210930095956330.png)

有一处错误，应该为http://127.0.0.1:3000/

![image-20210930100057413](../Typora图片/typora-user-images/image-20210930100057413.png)

![image-20210930100933967](../Typora图片/typora-user-images/image-20210930100933967.png)

在base.js最下面添加导出Base

![image-20210930101216673](../Typora图片/typora-user-images/image-20210930101216673.png)

![image-20210930101414684](../Typora图片/typora-user-images/image-20210930101414684.png)

在home-model.js构造函数中添加super();

ES6中423页有详细的知识点

suoer作为函数调用时代表父类的构造函数，ES6要求，子类的构造函数必须执行一次super，这是必须的，不然报错。

![image-20210930101506432](../Typora图片/typora-user-images/image-20210930101506432.png)



![image-20210930102920334](../Typora图片/typora-user-images/image-20210930102920334.png)

home-model.js

![image-20210930104045152](../Typora图片/typora-user-images/image-20210930104045152.png)

 **callBack && callBack(res)**

相当于(JavaScript基本功 简写 )

```
if(callBack){
callBack(res)
}
```

![image-20210930105131392](../Typora图片/typora-user-images/image-20210930105131392.png)

<img src="../Typora图片/typora-user-images/image-20210930105348055.png" alt="image-20210930105348055" style="zoom:80%;" />

控制台显示

<img src="../Typora图片/typora-user-images/image-20210930105407340.png" alt="image-20210930105407340" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930105926206.png" alt="image-20210930105926206" style="zoom:67%;" />

![](../Typora图片/typora-user-images/image-20210930110507905.png)

**使用数据绑定将数据显示在UI上**

<img src="../Typora图片/typora-user-images/image-20210930111553597.png" alt="image-20210930111553597" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930111613109.png" alt="image-20210930111613109" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930111621186.png" alt="image-20210930111621186" style="zoom:67%;" />



最终完整代码：

<img src="../Typora图片/typora-user-images/image-20210930142223468.png" alt="image-20210930142223468" style="zoom:80%;" />

<img src="../Typora图片/typora-user-images/image-20210930141943518.png" alt="image-20210930141943518" style="zoom:70%;" />

<img src="../Typora图片/typora-user-images/image-20210930142308764.png" alt="image-20210930142308764" style="zoom:79%;" />

<img src="../Typora图片/typora-user-images/image-20210930142439956.png" alt="image-20210930142439956" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930142455410.png" alt="image-20210930142455410" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930142513228.png" alt="image-20210930142513228" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930142631446.png" alt="image-20210930142631446" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930142615454.png" alt="image-20210930142615454" style="zoom:70%;" />

<img src="../Typora图片/typora-user-images/image-20210930151251151.png" alt="image-20210930151251151" style="zoom:80%;" />

<img src="../Typora图片/typora-user-images/image-20210930142644225.png" alt="image-20210930142644225" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930142719882.png" alt="image-20210930142719882" style="zoom:67%;" />

![image-20210930142735627](../Typora图片/typora-user-images/image-20210930142735627.png)

<img src="../Typora图片/typora-user-images/image-20210930142757060.png" alt="image-20210930142757060" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930142819284.png" alt="image-20210930142819284" style="zoom:67%;" />

![image-20210930142809016](../Typora图片/typora-user-images/image-20210930142809016.png)

<img src="../Typora图片/typora-user-images/image-20210930142828821.png" alt="image-20210930142828821" style="zoom:67%;" />

完成。来自tP5+小程序得第十一章前端部分：前端框架构建与令牌管理

11-3使用ES6构建面向对象的JS代码及访问服务器API获取数据

11-4异步回调与箭头函数

11-5构建请求基类（重要）

11-6请求基类的应用

腾讯文档：

# **分层描述**

在utils文件夹添加两个js分别是config和base，用来封装请求。

具体class要去看ES6中的class基础知识，温习。

### **一、测试**

1. #### **编写config.js**

**配置接口前缀（全局url）**

​                 ![img](https://docimg1.docs.qq.com/image/isEfedCWK53qARj8imBd3Q.png?w=849&h=369)        

1. #### **编写base.js**

**封装请求**

定义一个Base类，并且写一个请求方法

定义形参params，用来接收定义一个并继承了base类的类名的参数。

将请求方式、url和数据等传给wx.request函数



​                 ![img](https://docimg9.docs.qq.com/image/ohC74Qe-qgjvTpQnitnY-A.png?w=923&h=609)        

​                 ![img](https://docimg7.docs.qq.com/image/6-8lE6zsyV22trYEZpI0LA.png?w=564&h=417)        

​                 ![img](https://docimg1.docs.qq.com/image/H_43iKjVjWtPa_iq7RuHIA.png?w=668&h=691)        

res.data是下面的                 ![img](https://docimg5.docs.qq.com/image/5iT0v1CdKEhi2aMHUbxxLw.png?w=1235&h=378)        



1. #### **编写home-model.js**

**编写请求获取指定的接口数据**

在home文件夹中添加一个home-model.js

​                  ![img](https://docimg8.docs.qq.com/image/g4Z3xcAZY-d9SYKOOTPbmA.png?w=481&h=244)        

​                 ![img](https://docimg8.docs.qq.com/image/ajf39HIERIlP1PZRxEc23w.png?w=566&h=189)        



重要：

 if(callBack){

callBack(res)

}

相当于(JavaScript基本功 简写 )

callBack && callBack(res)



1. #### **编写home.js**

**数据绑定**

​                 ![img](https://docimg7.docs.qq.com/image/MPUQCIKDVYkELqR5um9_kw.png?w=334&h=193)        

调用home-model.js的方法，获取列表的数据

​                 ![img](https://docimg7.docs.qq.com/image/xk3tJu9hcK114wUsJ7tfAQ.png?w=437&h=313)        

测试

​                 ![img](https://docimg8.docs.qq.com/image/xch62vgeHNxL6DnM_5YmhQ.png?w=491&h=154)        

渲染成功。



我们可以用extends关键字扩展一个类并继承它的行为。在构造函数中，我们也 
可以通过super关键字引用父类的构造函数







**跳转指定id的数据展示在详细页**

home.wxml

:smiling_imp: 获取绑定事件元素的ID 

<img src="../Typora图片/typora-user-images/image-20210930153726311.png" alt="image-20210930153726311" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930153802870.png" alt="image-20210930153802870" style="zoom:67%;" />

在app.json

新建一个详细页 detail

<img src="../Typora图片/typora-user-images/image-20210930154011931.png" alt="image-20210930154011931" style="zoom:67%;" />

home.js  view里添加一个bindtap="todetail"

![image-20210930154044037](../Typora图片/typora-user-images/image-20210930154044037.png)

<img src="../Typora图片/typora-user-images/image-20210930162612049.png" alt="image-20210930162612049" style="zoom:67%;" />

获取元素上的绑定的值

base.js

<img src="../Typora图片/typora-user-images/image-20210930163433180.png" alt="image-20210930163433180" style="zoom:67%;" />

<img src="../Typora图片/typora-user-images/image-20210930164040995.png" alt="image-20210930164040995" style="zoom:67%;" />

home.js

<img src="../Typora图片/typora-user-images/image-20210930164350498.png" alt="image-20210930164350498" style="zoom:67%;" />

测试结果：正常跳转

detail文件夹里新建一个detail-model.js

:satisfied: **detail-model.js**

- 引入base.js的Base类
- 定义一个Detail类 继承Base的所有方法和属性
- 获取详细页的数据

```js
// 引入base.js 的Base类
import {Base} from '../../utils/base.js'
class Detail extends Base{
    constructor(){
    super();
    }
    // 获取详细页的数据
    //对应详细页的id号
    getDetailData(id,callBack){
        var params={
            url:'listdetail?id='+id,
             sCallBack:(data)=>{
                callBack && callBack(data.listdata);    //data等同于res.data  data.listdata等同于res.data.listdata
            }
        }
        this.request(params);
    }
}
export {Detail}
```

:satisfied: **detail.js**

- 引入detail-model.js 的Detail类
- 实例化detail对象
- 获取页面传来的options的id值
- options.id赋值给this.data.id
- 绑定id的数据

```js
// pages/detail/detail.js
//需要引入detail-model.js Detail
import {Detail} from 'detail-model.js';
var detail=new Detail();
Page({
    data: {
    },
    // 页面传来的options值
    onLoad: function (options) {
        var id=options.id;
        console.log('收到传过来的id值为：'+id)  
        this.data.id=id
        this.getDetail();
    },
   //绑定指定id的值
    getDetail(){
        detail.getDetailData(this.data.id,(data)=>{
            let Data=data.filter(item => {
                return item.id == this.data.id
              })
            this.setData({
                detailData:Data[0]
            })
        })
    }
})
```

